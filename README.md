# Multi Source Config

When developing many types of applications (in particular web apps), it's often
[best practice](http://12factor.net/config) to pull certain settings from
environment variables. This can be a bit cumbersome to manage during
development. There are a bunch of different approaches to helping manage this
stuff, perhaps the most famous being
[dotenv](https://github.com/bkeepers/dotenv), but I wanted a bit more
flexibility, while also remaining simple, than I could find in current
offerings.

So this module can pull and merge settings from multiple sources to make
applications easier to manage and deploy. It provides a central place to
declare configurable behavior. It's not exactly thin on dependencies, but they
generally aren't unreasonable for most projects.

Initial version was extracted from Yesod.Default.Config2 and as such, the
[Yesod wiki] has some relevant discussion. Thanks to Michael Snoyman for doing
all the hard work, I just copied and pasted.

[Yesod wiki]: https://github.com/yesodweb/yesod/wiki/Configuration
